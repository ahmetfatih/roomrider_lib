#!/usr/bin/env python
from roomrider_lib.roomrider import Robot
import rospy
import math


robot = Robot("roomrider")

while robot.is_ok():
    robot.move_forward()
    robot.move_forward()
    robot.move_forward()
    robot.turn_right()
